package com.mm.githubtechnicaltask.repository

import com.mm.githubtechnicaltask.model.Commit
import com.mm.githubtechnicaltask.model.db.ProjectDAO
import com.mm.githubtechnicaltask.model.db.mappers.CommitCacheMapper
import com.mm.githubtechnicaltask.model.network.api.GitHubService
import com.mm.githubtechnicaltask.model.network.mappers.CommitNetworkMapper
import com.mm.githubtechnicaltask.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class CommitsRepository
constructor(
    private val projectDAO: ProjectDAO,
    private val gitHubService: GitHubService,
    private val commitsCacheMapper: CommitCacheMapper,
    private val networkMapper: CommitNetworkMapper
) {

    fun getCommits(owner: String, repo: String): Flow<DataState<List<Commit>>> = flow {
        emit(DataState.Loading)
        try {
            val networkCommits = gitHubService.getCommits(owner, repo)
            val commits = networkMapper.mapFromEntityList(networkCommits, "$owner/$repo")
            for (commit in commits) {
                projectDAO.insertCommit(commitsCacheMapper.mapToEntity(commit))
            }
            emit(DataState.Success(commits))
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }
}