package com.mm.githubtechnicaltask

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GitHubTechnicalTaskApplication: Application()