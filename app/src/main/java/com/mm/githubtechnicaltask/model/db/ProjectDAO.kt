package com.mm.githubtechnicaltask.model.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mm.githubtechnicaltask.model.db.entities.CommitCacheEntity
import com.mm.githubtechnicaltask.model.db.entities.RepositoryCacheEntity

@Dao
interface ProjectDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRepository(repoEntity: RepositoryCacheEntity)

    @Query("SELECT * FROM repositories")
    suspend fun getCachedRepositories(): List<RepositoryCacheEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCommit(commitEntity: CommitCacheEntity)

    @Query("SELECT * FROM commits WHERE repoName LIKE :repoName")
    suspend fun getCachedCommits(repoName: String): List<CommitCacheEntity>
}