package com.mm.githubtechnicaltask.model.network.networkentities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RepositoryNetworkEntity(
    @Expose
    @SerializedName("id")
    var id: String,

    @Expose
    @SerializedName("full_name")
    var name: String
)