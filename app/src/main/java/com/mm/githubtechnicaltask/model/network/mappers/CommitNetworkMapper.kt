package com.mm.githubtechnicaltask.model.network.mappers

import com.mm.githubtechnicaltask.model.Commit
import com.mm.githubtechnicaltask.model.network.networkentities.AuthorNetworkEntity
import com.mm.githubtechnicaltask.model.network.networkentities.CommitNetworkEntity
import com.mm.githubtechnicaltask.model.network.networkentities.CommitResponseNetworkEntity
import com.mm.githubtechnicaltask.utils.EntityMapper
import javax.inject.Inject

class CommitNetworkMapper
@Inject
constructor() : EntityMapper<CommitResponseNetworkEntity, Commit> {
    override fun mapFromEntity(entity: CommitResponseNetworkEntity): Commit {
        return Commit(sha = entity.sha, message = entity.commit.message, authorName = entity.commit.author.name, repoName = null)
    }

    override fun mapToEntity(domainModel: Commit): CommitResponseNetworkEntity {
       return CommitResponseNetworkEntity(
           sha = domainModel.sha,
           commit = CommitNetworkEntity(AuthorNetworkEntity(domainModel.authorName), domainModel.message)
       )
    }

    fun mapFromEntityList(entities: List<CommitResponseNetworkEntity>, repoName: String): List<Commit> {
        return entities.map {
            val commit = mapFromEntity(it)
            commit.repoName = repoName
            commit
        }
    }
}