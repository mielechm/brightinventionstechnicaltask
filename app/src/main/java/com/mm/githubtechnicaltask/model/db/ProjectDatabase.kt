package com.mm.githubtechnicaltask.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mm.githubtechnicaltask.model.db.entities.CommitCacheEntity
import com.mm.githubtechnicaltask.model.db.entities.RepositoryCacheEntity

@Database(
    entities = [RepositoryCacheEntity::class, CommitCacheEntity::class],
    version = 4,
    exportSchema = false
)
abstract class ProjectDatabase : RoomDatabase() {

    abstract fun projectDao(): ProjectDAO

    companion object {
        const val DATABASE_NAME = "project_db"
    }

}