package com.mm.githubtechnicaltask.model.network.api

import com.mm.githubtechnicaltask.model.network.networkentities.CommitResponseNetworkEntity
import com.mm.githubtechnicaltask.model.network.networkentities.RepositoryNetworkEntity
import retrofit2.http.GET
import retrofit2.http.Path

interface GitHubService {

    @GET("/repos/{owner}/{repo}")
    suspend fun getRepo(
        @Path("owner") owner: String,
        @Path("repo") repo: String
    ): RepositoryNetworkEntity

    @GET("/repos/{owner}/{repo}/commits")
    suspend fun getCommits(
        @Path("owner") owner: String,
        @Path("repo") repo: String
    ): List<CommitResponseNetworkEntity>
}