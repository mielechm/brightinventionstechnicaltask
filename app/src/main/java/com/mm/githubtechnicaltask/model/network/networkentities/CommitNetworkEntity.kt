package com.mm.githubtechnicaltask.model.network.networkentities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CommitNetworkEntity(
    @Expose
    @SerializedName("author")
    var author: AuthorNetworkEntity,
    @Expose
    @SerializedName("message")
    var message: String
)