package com.mm.githubtechnicaltask.model.db.mappers

import com.mm.githubtechnicaltask.model.Repo
import com.mm.githubtechnicaltask.model.db.entities.RepositoryCacheEntity
import com.mm.githubtechnicaltask.utils.EntityMapper
import javax.inject.Inject

class RepositoryCacheMapper
@Inject
constructor() : EntityMapper<RepositoryCacheEntity, Repo> {
    override fun mapFromEntity(entity: RepositoryCacheEntity): Repo {
        return Repo(
            id = entity.id,
            name = entity.name
        )
    }

    override fun mapToEntity(domainModel: Repo): RepositoryCacheEntity {
        return RepositoryCacheEntity(
            id = domainModel.id,
            name = domainModel.name
        )
    }

    fun mapFromEntityList(entities: List<RepositoryCacheEntity>): List<Repo> {
        return entities.map {
            mapFromEntity(it)
        }
    }


}