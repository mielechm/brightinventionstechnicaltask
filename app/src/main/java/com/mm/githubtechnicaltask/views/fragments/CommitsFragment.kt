package com.mm.githubtechnicaltask.views.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.mm.githubtechnicaltask.R
import com.mm.githubtechnicaltask.model.Commit
import com.mm.githubtechnicaltask.model.Repo
import com.mm.githubtechnicaltask.utils.DataState
import com.mm.githubtechnicaltask.viewmodel.CommitsStateEvent
import com.mm.githubtechnicaltask.viewmodel.CommitsViewModel
import com.mm.githubtechnicaltask.views.adapters.CommitAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_commits.*

@AndroidEntryPoint
class CommitsFragment : Fragment() {
    private lateinit var container: ConstraintLayout
    private lateinit var adapter: CommitAdapter

    private val viewModel: CommitsViewModel by viewModels()

    private val selectedCommits = mutableListOf<Commit>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_commits, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        container = view as ConstraintLayout

        val toolbar = (activity as AppCompatActivity).supportActionBar
        toolbar?.setDisplayHomeAsUpEnabled(true)

        val repository = arguments?.get("repo") as Repo
        repo_id.text =
            String.format(resources.getString(R.string.repository_id_label), repository.id)

        subscribeObservers()
        val owner = repository.name.split("/")[0]
        val repoName = repository.name.split("/")[1]
        viewModel.setStateEvent(CommitsStateEvent.GetRemoteCommitsStateEvent, owner, repoName)

        share_commits_button.setOnClickListener {
            shareCommit()
        }
    }

    private fun subscribeObservers() {
        viewModel.commitsDataState.observe(requireActivity(), Observer { dataState ->
            when (dataState) {
                is DataState.Success<List<Commit>> -> {
                    fetchCommits(dataState.data)
                    showProgressBar(false)
                }
                is DataState.Loading -> {
                    showProgressBar(true)
                }
                is DataState.Error -> {
                    showProgressBar(false)
                    Toast.makeText(
                        requireContext(),
                        dataState.exception.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    private fun showProgressBar(isDisplayed: Boolean) {
        progress_bar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun fetchCommits(commits: List<Commit>) {
        adapter = CommitAdapter(commits) { commit ->
            if (selectedCommits.contains(commit))
                selectedCommits.remove(commit)
            else
                selectedCommits.add(commit)

            adapter.notifyDataSetChanged()
        }
        commits_recycler_view.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(context)
            adapter = this@CommitsFragment.adapter
        }
        commits_recycler_view.addItemDecoration(
            DividerItemDecoration(
                commits_recycler_view.context,
                DividerItemDecoration.VERTICAL
            )
        )
    }

    private fun shareCommit() {
        if (selectedCommits.isNotEmpty()) {
            val sendIntent = Intent(Intent.ACTION_SEND)
            val shareBody = ""
            for (commit in selectedCommits) {
                shareBody + "Message: " + commit.message + "\nSHA: " + commit.sha + "\nAuthor: " + commit.authorName + "\n"
            }
            sendIntent.type = "text/plain"
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Commit")
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
            startActivity(Intent.createChooser(sendIntent, "Share commit"))
        }
    }
}