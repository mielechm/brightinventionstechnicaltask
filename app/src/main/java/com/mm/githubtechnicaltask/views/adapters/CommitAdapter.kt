package com.mm.githubtechnicaltask.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mm.githubtechnicaltask.R
import com.mm.githubtechnicaltask.model.Commit
import kotlinx.android.synthetic.main.item_commit.view.*

class CommitAdapter(private val commits: List<Commit>, private val clickCallback: (Commit) -> Unit): RecyclerView.Adapter<CommitAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_commit, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = commits.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val commit = commits[position]

        holder.view.message_text.text = commit.message
        holder.view.sha_text.text = commit.sha
        holder.view.author_text.text = commit.authorName

        holder.itemView.setOnClickListener {
            clickCallback.invoke(commit)
        }
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}